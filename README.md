# Front-end Test for DevDynamics

Front-end assignment for DevDynamics
Generated Project using Vite + react-ts.

---

[Live on Vercel](https://dev-dynamics.vercel.app/)

---

![Web Screenshot](github/screenshot-web.png)

---
### 💡 Running Locally

```bash
$ git clone git@gitlab.com:Vynex/DevDynamics.git

$ cd DevDynamics
$ yarn

$ yarn dev
```
