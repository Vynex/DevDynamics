import { css } from "@emotion/react";

const styles = {
  header: css({
    backgroundColor: "#4D4CFF",
    color: "#F3F3F3",

    padding: "1rem 2rem",
    fontWeight: 700,
    fontSize: 18,
  }),

  link: css({
    width: "fit-content",
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    columnGap: "8px",
  }),
};

export default styles;
