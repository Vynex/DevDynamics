import BrandIcon from "@/assets/brand.svg?react";
import styles from "./styles";

export default function Header(): React.ReactElement {
  const onClick = () => {
    const aEl = document.createElement("a");
    aEl.setAttribute("href", "https://devdynamics.ai/");
    aEl.setAttribute("target", "_blank");
    aEl.click();
  };

  return (
    <header css={styles.header}>
      <div css={styles.link} onClick={onClick}>
        <BrandIcon height={32} width={32} />
        DevDynamics
      </div>
    </header>
  )
}
