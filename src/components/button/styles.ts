const styles = {
  button: {
    backgroundColor: "transparent",
    padding: "4px 8px",
    borderRadius: "8px",

    font: "inherit",
    display: "flex",
    alignItems: "center",

    border: "1px solid transparent",
    cursor: "pointer",
    transition: "box-shadow 0.25s ease-in",

    "&:hover": {
      boxShadow: "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px",
      backgroundColor: "#FFFFFF",
    },
    "&:focus": {
      outline: "1px auto -webkit-focus-ring-color",
    },
  },

  active: {
    color: "#4D4CFF",
    boxShadow: "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px",
    backgroundColor: "#FFFFFF",

    "&:hover": {
      boxShadow: "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 2px 2px",
      backgroundColor: "#FFFFFF",
    },
  },
};

export default styles;
