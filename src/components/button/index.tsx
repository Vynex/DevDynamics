import { ButtonHTMLAttributes } from "react";
import styles from "./styles";

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  active?: boolean;
  children?: React.ReactNode;
};

export default function Button({ active, children, ...props }: ButtonProps): React.ReactElement {
  let css = styles.button;
  if (active) css = { ...css, ...styles.active };

  return (
    <button css={css} {...props}>
      {children}
    </button>
  );
}
