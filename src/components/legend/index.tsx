import Button from "../button";
import styles from "./styles";

type Props = {
  metadata: { [key: string]: string };
  activeKeys: Set<string>;
  onClick: (key: string) => void;
};

export default function Legend({ metadata, activeKeys, onClick }: Props): React.ReactElement {
  const renderLegends = ()  => {
    const render = [];

    for (const key in metadata) {
      render.push(
        <Button onClick={() => onClick(key)} active={activeKeys.has(key)} key={render.length} style={{ color: "unset" }}>
          <div css={styles.badge} style={{ background: metadata[key] }} />
          <span>{key}</span>
        </Button>
      );
    }

    return render;
  };

  return (
    <section css={styles.container}>
      {renderLegends()}
    </section>
  );
}
