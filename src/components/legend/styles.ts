import { css } from "@emotion/react";

const styles = {
  container: css({
    display: "flex",
    marginTop: 16,
    columnGap: 10,
  }),

  badge: css({
    width: 10,
    aspectRatio: 1,
    borderRadius: 5,
    marginRight: 8,
  }),
};

export default styles;
