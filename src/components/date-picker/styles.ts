import { css } from "@emotion/react";

const styles = {
  container: css({
    display: "flex",
    alignItems: "center",
    columnGap: "8px",

    fontSize: "14px",
    fontWeight: "500",
  }),

  date: css({
    cursor: "pointer",
    "&:hover": {
      textDecoration: "underline",
    },
  }),
};

export default styles;
