import { CalendarIcon, ChevronLeftIcon, ChevronRightIcon } from "@/assets/icons";
import styles from "./styles";
import Button from "../button";

export default function DatePicker(): React.ReactElement {
  return (
    <div css={styles.container}>
      <CalendarIcon />

      <Button>
        <ChevronLeftIcon />
      </Button>

      <span css={styles.date}>May 6 - May 19</span>

      <Button>
        <ChevronRightIcon />
      </Button>
    </div>
  )
}
