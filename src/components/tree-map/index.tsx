import ReactApexChart from "react-apexcharts";

type Props = {
  height: number;
  dataset: Array<{
    data: Array<{
      x: string | number;
      y: string | number;
      fillColor: string;
    }>;
  }>;
};

export default function TreeMap({ height = 175, dataset = [] }: Props): React.ReactElement {
  return (
    <ReactApexChart
      type="treemap"
      height={height}
      series={dataset}
      options={{
        dataLabels: { formatter: (_, opts) => opts.value || "", style: { fontSize: "18px", fontFamily: "Nunito Sans" } },
        legend: { show: false },
        chart: { toolbar: { show: false } },
        fill: { type: "solid", opacity: 1 },
        plotOptions: {
          treemap: {
            borderRadius: 4,
          }
        },
        states: {
          normal: {
            filter: { type: "none", value: 0 },
          },
          hover: {
            filter: { type: "darken", value: 0.75 },
          },
        },
      }}
    />
  )
}
