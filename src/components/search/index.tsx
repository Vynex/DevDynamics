import { SearchIcon } from "@/assets/icons";
import styles from "./styles";

type Props = {
  value: string;
  onChange: (value: string) => void;
};

export default function Search({ value, onChange }: Props): React.ReactElement {
  return (
    <div css={styles.container}>
      <SearchIcon style={{ position: "absolute", left: 12 }} />

      <input
        css={styles.input}
        type="text"
        value={value}
        onChange={(event) => onChange(event.target.value)}
        placeholder="Search for Author"
      />
    </div>
  );
}
