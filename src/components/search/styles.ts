import { css } from "@emotion/react";

const styles = {
  container: css({
    width: "40%",
    position: "relative",
    display: "flex",
    alignItems: "center",
  }),

  input: css({
    width: "100%",
    minWidth: 500,
    borderRadius: 16,
    padding: "4px 12px",
    paddingLeft: 38,
    border: "1px solid #CCCCCC",
    font: "inherit",
  }),
};

export default styles;
