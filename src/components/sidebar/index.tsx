import { useState } from "react";

import { HomeIcon, WorkFlowIcon } from "@/assets/icons";
import styles from "./styles";
import Button from "../button";

const LINKS = [
  { name: "Home", Icon: HomeIcon },
  { name: "Worklog", Icon: WorkFlowIcon },
];

export default function Sidebar(): React.ReactElement {
  const [ active ] = useState(1);

  return (
    <aside css={styles.aside}>
      {LINKS.map((link, index) => (
        <Button active={index === active} key={index}>
          <link.Icon height={24} width={24} />
        </Button>
      ))}
    </aside>
  );
}
