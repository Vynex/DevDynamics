import { css } from "@emotion/react";

const styles = {
  aside: css({
    minWidth: "64px",
    height: "100vh",
    backgroundColor: "#FDFDFD",

    border: "2px solid #E5E5E5",
    borderTop: "none",
    borderBottom: "none",

    padding: "24px 0",
    display: "flex",
    flexDirection: "column",
    rowGap: "24px",
    alignItems: "center",
  }),
};

export default styles;
