export { default as ChevronDownIcon } from "./chevron-down.svg?react";
export { default as ChevronLeftIcon } from "./chevron-left.svg?react";
export { default as ChevronRightIcon } from "./chevron-right.svg?react";

export { default as HomeIcon } from "./home.svg?react";
export { default as WorkFlowIcon } from "./workflow.svg?react";
export { default as CalendarIcon } from "./calendar.svg?react";
export { default as FlameIcon } from "./flame.svg?react";
export { default as PeopleIcon } from "./people.svg?react";
export { default as SearchIcon } from "./search.svg?react";