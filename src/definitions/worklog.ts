export type AuthorTotalActivityType = {
  name: string;
  value: string;
};

export type WorkLogItemType = {
  count: string;
  label: string;
  fillColor: string;
};

export type WorkLogRow = {
  name: string;
  totalActivity: Array<AuthorTotalActivityType>;

  dayWiseActivity: Array<{
    date: string;
    items: {
      children: Array<WorkLogItemType>;
    };
  }>;

  activeDays: {
    days: number;
    isBurnOut: boolean;
    insight: Array<string>;
  };
};

export type AuthorActivityMetaType = {
  label: string;
  fillColor: string;
};

export type AuthorWorklogType = {
  activityMeta: Array<AuthorActivityMetaType>;
  rows: Array<WorkLogRow>;
};
