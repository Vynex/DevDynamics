import Header from "@/components/header";
import Sidebar from "@/components/sidebar";
import styles from "./styles";

type AppLayoutProps = {
  children: React.ReactNode;
};

export default function AppLayout({ children }: AppLayoutProps): React.ReactElement {
  return <>
    <Header />

    <section css={styles.section}>
      <Sidebar />

      <main css={styles.main}>
        {children}
      </main>
    </section>
  </>;
}
