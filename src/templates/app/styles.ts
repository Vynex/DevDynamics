import { css } from "@emotion/react";

const styles = {
  section: css({
    display: "flex",
  }),

  main: css({
    height: "calc(100vh - 64px)",
    width: "100%",
    display: "flex",
    flexDirection: "column",

    padding: "2rem 4rem",
    overflow: "auto",
  }),
};

export default styles;
