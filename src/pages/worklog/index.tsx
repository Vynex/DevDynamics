import { useEffect, useMemo, useState } from "react";

import DatePicker from "@/components/date-picker";
import Legend from "@/components/legend";
import Search from "@/components/search";
import Table from "./components/table";

import { fetchWorklog } from "@/services";
import styles from "./styles";

import { AuthorWorklogType, WorkLogRow } from "@/definitions/worklog";

export default function WorkLog() {
  const [ isLoading, setIsLoading ] = useState(true);
  const [ worklog, setWorklog ] = useState<AuthorWorklogType | null>(null);

  const [ selectedActivities, setSelectedActivities ] = useState<Set<string>>(new Set());
  const [ authorQuery, setQuery ] = useState<string>("");

  useEffect(() => {
    const _fetchData = async () => {
      setIsLoading(true);

      try {
        const result: { data: { AuthorWorklog: AuthorWorklogType } } = await fetchWorklog();

        const selectedKeys = new Set<string>();
        result.data.AuthorWorklog.activityMeta.forEach((meta) => {
          selectedKeys.add(meta.label);
        });

        setWorklog(result.data.AuthorWorklog);
        setSelectedActivities(selectedKeys);

      } catch (error) {
        // console.error("Error fetching worklog", error);
      } finally {
        setIsLoading(false);
      }
    };

    _fetchData();
  }, []);

  const metadata = useMemo(() => {
    if (!worklog) return {};

    const metaMap: { [key: string]: string } = {};
    worklog.activityMeta.forEach((meta) => {
      metaMap[meta.label] = meta.fillColor;
    });

    return metaMap;
  }, [ worklog ]);

  const handleLegendClick = (activity: string) => {
    const newSelection = new Set<string>(selectedActivities);
    if (newSelection.has(activity)) newSelection.delete(activity);
    else newSelection.add(activity);

    setSelectedActivities(newSelection);
  };

  const handleAuthorFilter = (query: string) => {
    setQuery(query.trim());
  };

  // return graph data based on current filters and selections
  const getFilteredData = (rawData: Array<WorkLogRow>) => {
    const tableData: Array<WorkLogRow> = [];

    rawData.forEach(row => {
      if (authorQuery && !row.name.toLowerCase().includes(authorQuery.toLowerCase())) return;

      tableData.push({ ...row, dayWiseActivity: row.dayWiseActivity.map(day => {
        return ({ ...day, items: { ...day.items, children: day.items.children.filter(item => {
          return selectedActivities.has(item.label);
        }) } });
      }) });
    });

    return tableData;
  };

  return <>
    <header>
      <h1>Work Log</h1>
      <div>
        Stay on top of all the work. Get insights on how your team manages their time.
      </div>
    </header>

    <main>
      <section css={styles.filters}>
        <DatePicker />
        <div css={styles.vr} />
        <Search value={authorQuery} onChange={handleAuthorFilter} />
      </section>

      {isLoading ? (<div>Loading...</div>) : (<>
        <Legend metadata={metadata} activeKeys={selectedActivities} onClick={handleLegendClick} />
        <Table data={getFilteredData(worklog!.rows)} />
      </>)}
    </main>
  </>;
}
