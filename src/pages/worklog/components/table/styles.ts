import { css } from "@emotion/react";

const styles = {
  table: css({
    margin: "32px 0",
    display: "flex",
    flexDirection: "column",
    maxWidth: "calc(100vw - 4rem - 64px)",

    overflowX: "auto",
    overflowY: "hidden",
  }),

  row: css({
    display: "flex",
    borderLeft: "none",
    borderRight: "none",
  }),

  list: css({
    padding: "2px 8px",
    display: "flex",
    flexDirection: "column",
    fontSize: 12,
  }),

  listItem: css({
    display: "flex",
  }),

  key: css({
    minWidth: "20px",
    fontWeight: 800,
  }),

  value: css({
    fontWeight: 600,
  }),

  author: css({
    flex: 3,
    minWidth: "300px",
    border: "0.5px solid #E0E0E0",
    borderLeft: "none",

    padding: "16px 24px",
  }),

  headCell: css({
    flex: 1,
    minWidth: "200px",
    minHeight: "100px",
    border: "0.5px solid #E0E0E0",
    borderTop: "none",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",

    ":last-child": {
      borderRight: "none",
    },
  }),

  dataCell: css({
    flex: 1,
    minWidth: "200px",
    minHeight: "175px",
    border: "0.5px solid #E0E0E0",

    display: "flex",
    alignItems: "center",

    ":last-child": {
      borderRight: "none",
    },
  }),

  dateContainer: {
    disply: "flex",
  },

  headDate: css({
    fontWeight: 600,
    fontSize: 32,
  }),

  headMonth: css({
    fontWeight: 500,
    fontSize: 18,
  }),
};

export default styles;
