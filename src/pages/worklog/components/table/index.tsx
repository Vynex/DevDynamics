import { useMemo } from "react";
import TreeMap from "@/components/tree-map";
import { FlameIcon } from "@/assets/icons";
import styles from "./styles";

import { WorkLogItemType, WorkLogRow } from "@/definitions/worklog";

type Props = {
  data: Array<WorkLogRow>
};

export default function Table({ data = [] }: Props)  {
  const headers = useMemo(() => {
    if (!data.length) return [];
    return data[0].dayWiseActivity.map(column => new Date(column.date));
  }, [ data ]);

  const getTableCell = (series: Array<WorkLogItemType>) => {
    const dataset =  [{
      data: series.map((item) => ({ x: item.label, y: item.count, fillColor: item.fillColor }))
    }];

    return <TreeMap height={150} dataset={dataset} />;
  };

  const getDateHeader = (date: Date, index: number) => {
    return (
      <div key={index} css={styles.headCell}>
        <div css={styles.dateContainer}>
          <span css={styles.headDate}> {date.getDate()} </span>
          <span css={styles.headMonth}>{new Intl.DateTimeFormat("en-IN", { month: "long" }).format(date)}</span>
        </div>
      </div>
    )
  };

  return (
    <section css={styles.table}>
      <div css={styles.row}>
        <div css={styles.author} style={{ borderTop: "none" }} />
        {headers.map((column, i) => getDateHeader(column, i))}
      </div>

      {data.map((row, i) => (
        <div key={row.name} css={styles.row}>
          <div css={styles.author} style={{ borderBottom: (i === data.length - 1) ? "none" : "unset" }}>
            <div>
              <h4>{row.name} {row.activeDays.isBurnOut && <FlameIcon fill="#F27D0C" />}</h4>
            </div>

            <div css={styles.list}>
              {row.totalActivity.map(item => +item.value ? (
                <div key={item.name} css={styles.listItem}>
                  <span css={styles.key}>{item.value}</span>
                  <span css={styles.value}>{item.name}</span>
                </div>
              ) : null)}
            </div>
          </div>

          {row.dayWiseActivity.map((column, j) => (
            <div key={column.date} css={styles.dataCell} style={{ borderBottom: (j === row.dayWiseActivity.length - 1) ? "none" : "unset" }}>
              {getTableCell(column.items.children)}
            </div>
          ))}
        </div>
      ))}

    </section>
  );
}
