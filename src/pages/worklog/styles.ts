const styles = {
  filters: {
    margin: "8px 0",
    marginTop: "32px",
    height: "48px",
    display: "flex",
    alignItems: "center",

    border: "1px solid #E0E0E0",
    borderLeft: "none",
    borderRight: "none",
    columnGap: "16px",
  },

  vr: {
    width: "1.5px",
    height: "60%",
    borderRadius: "2px",
    backgroundColor: "#E0E0E0",
  }

};

export default styles;
