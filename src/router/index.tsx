// App Router can go here

import WorkLog from "../pages/worklog";
import AppLayout from "../templates/app";

export default function Router(): React.ReactElement {
  return (
    <AppLayout>
      <WorkLog />
    </AppLayout>
  );
}
