const BASE_URL = "https://run.mocky.io/v3/";

export async function fetchWorklog() {
  const endpoint = "d009d168-9b3d-4a74-be40-21eb52898fef";
  const response = await fetch(BASE_URL + endpoint);

  try {
    const json = await response.json();
    if (response.status !== 200) throw json;
    else return json;

  } catch(error) {
    console.error("fetchWorklog:", endpoint, error);
  }
}
